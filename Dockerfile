FROM python:latest

ADD start.sh . 
ADD index.html .
ADD tests ./tests
ENTRYPOINT ["./start.sh"]
