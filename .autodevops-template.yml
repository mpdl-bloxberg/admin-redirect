.auto-deploy:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:latest"


image: "quay.io/buildah/stable:v1.23.1"

variables:
  PROJECT_NAME: template
  # persistent volume "name:mountpoint:space [...]"
  PERSISTENT_VOLUMES: ""
  # secrets "name:mountpoint [...]
  SECRETS: ""
  MAX_MEM: "1Gi"
  MAX_CPU: "1"
  
  APP_PORT: "8545" # Specify the main communication port to the App. This port will be used to determine if service is online. (default 5000)     
  INGRESS_PORT: "8545" # Specify the port exposed externally via nginx as a web app. (default 5000)
  INTERNAL_PORTS: "8546" # Specify additional ports exposed to the cluster, separated by space. (default none)
  EXTERNAL_PORTS: "30303" # Specify additional ports exposed to the world, separated by space. (default node)
  

  POSTGRES_ENABLED: "false"
  STORAGE_DRIVER: "overlay"
  BUILDAH_FORMAT: "docker"
  BUILDAH_ISOLATION: "chroot"
  ROLLOUT_RESOURCE_TYPE: deployment
  KUBE_INGRESS_BASE_DOMAIN: test.bloxberg.org
  GIT_SUBMODULE_STRATEGY: recursive

  ADDITIONAL_HOSTS: ""


stages:
  - publish
  - staging
  - qa
  - production


# REGISTRY
1. registry:
  stage: publish
  tags:
    - nut
  script:
    - bloxberg-pipeline-template/bin/publish
  rules:
    - if: $CI_COMMIT_BRANCH             # Execute jobs when a new commit is pushed to master branch


# STAGING
# NUT
1. unit test:
  extends: .auto-deploy
  stage: staging
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - bloxberg-pipeline-template/bin/test
  environment:
    name: nut/staging
    url: https://$CI_PROJECT_PATH_SLUG.nut.$KUBE_INGRESS_BASE_DOMAIN


2. start stage:
  extends: .auto-deploy
  stage: staging
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - bloxberg-pipeline-template/bin/dns-record
    - auto-deploy deploy

  environment:
    name: nut/staging
    url: https://$CI_PROJECT_PATH_SLUG.nut.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: 3. stop stage
  when: manual


3. stop stage:
  extends: .auto-deploy
  stage: staging
  tags: 
    - thoth
  variables:
    GIT_STRATEGY: none
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy delete
  environment:
    name: nut/staging
    action: stop
  when: manual


# QA
# NUT
1. integration test:
  extends: .auto-deploy
  stage: qa
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - bloxberg-pipeline-template/bin/test
  environment:
    name: nut/qa
    url: https://$CI_PROJECT_PATH_SLUG.qa.nut.$KUBE_INGRESS_BASE_DOMAIN


2. start qa:
  extends: .auto-deploy
  stage: qa
  tags: 
    - nut
  needs:
    - 1. integration test
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - bloxberg-pipeline-template/bin/dns-record
    - bloxberg-pipeline-template/bin/deploy
  environment:
    name: nut/qa
    url: https://$PROJECT_NAME.qa.nut.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: 4. stop qa


3. end to end test:
  extends: .auto-deploy
  stage: qa
  tags: 
    - nut
  needs: 
    - 2. start qa
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - bloxberg-pipeline-template/bin/test
  environment:
    name: nut/qa
    url: https://$CI_PROJECT_PATH_SLUG.qa.nut.$KUBE_INGRESS_BASE_DOMAIN


4. stop qa:
  extends: .auto-deploy
  stage: qa
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy delete
    - bloxberg-pipeline-template/bin/stop
  environment:
    name: nut/qa
    action: stop
  when: manual

5. get logs:
  extends: .auto-deploy
  stage: qa
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - bloxberg-pipeline-template/bin/get-logs
  environment:
    name: nut/qa
  when: manual



# PRODUCTION


1. start prod:
  extends: .auto-deploy
  stage: production
  tags:
    - thoth
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - bloxberg-pipeline-template/bin/dns-record
    - bloxberg-pipeline-template/bin/deploy
  environment:
    name: thoth/production
    url: https://$PROJECT_NAME.prod.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: 3. stop prod
  when: manual


2. activate prod:
  extends: .auto-deploy
  stage: production
  tags:
    - thoth
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - bloxberg-pipeline-template/bin/dns-record
  environment:
    name: thoth/production
    url: https://$PROJECT_NAME.prod.$KUBE_INGRESS_BASE_DOMAIN
  when: manual


3. stop prod:
  extends: .auto-deploy
  stage: production
  tags: 
    - thoth
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy delete
    - bloxberg-pipeline-template/bin/stop
  environment:
    name: thoth/production
    action: stop
  when: manual


4. start backup prod:
  extends: .auto-deploy
  stage: production
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy create_secret
    - bloxberg-pipeline-template/bin/dns-record
    - bloxberg-pipeline-template/bin/deploy
  environment:
    name: nut/production
    url: https://$PROJECT_NAME.prod.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: 6. stop backup prod
  when: manual

5. activate backup prod:
  extends: .auto-deploy
  stage: production
  tags:
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - bloxberg-pipeline-template/bin/dns-record
  environment:
    name: nut/production
    url: https://$PROJECT_NAME.prod.$KUBE_INGRESS_BASE_DOMAIN
  when: manual

6. stop backup prod:
  extends: .auto-deploy
  stage: production
  tags: 
    - nut
  script:
    - source bloxberg-pipeline-template/bin/set-env
    - auto-deploy delete
    - bloxberg-pipeline-template/bin/stop
  environment:
    name: nut/production
    action: stop
  when: manual



